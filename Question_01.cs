using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Threading;

namespace Assignment6
{   
    class Program
    {
        static long f(int n)
		{
		   long sum = 0;

		   for (int i = 1; i < n; i++)
		   if (i % 3 == 0 || i % 5 == 0)
			  sum += i;

		   return sum;
		}

        static void Main(string[] args)
        {
            Stopwatch sw = new Stopwatch();
			sw.Start();
			long num = f(/* input */);
			sw.Stop();
			Console.WriteLine(sw.ElapsedMilliseconds);
			sw.Reset();
        }
    }
}
