using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Threading;

namespace Assignment6
{   
    class Program
    {
		static long f(int n)
		{
			long sum = 0;

			for (int i = 3; i < n; i += 3)
				sum += i;
			for (int i = 5; i < n; i += 5)
				if (i % 3 != 0)
					sum += i;
			return sum;
		}

		static void Main(string[] args)
		{
			long total = 0;
			Stopwatch sw = new Stopwatch();
			
			for (int i = 0; i < 5; i++)
			{
				sw.Start();
				long num = f(/* input */);
				sw.Stop();
				Console.WriteLine("Run 1 - {0} ms", sw.ElapsedMilliseconds);
				total += sw.ElapsedMilliseconds;
				sw.Restart();
			}
			
			Console.WriteLine("Average - {0} ms", total / 5);
		}
    }
}