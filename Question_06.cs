using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Threading;

namespace Assignment6
{   
    class Program
    {
        static void f(int n)
        {
            double sum1 = (n - 1) / 3;
            sum1 *= sum1 + 1;
            sum1 *= 1.5; //(3/2) 

            double sum2 = (n - 1) / 5;
            sum2 *= sum2 + 1;
            sum2 *= 2.5; //(5/2) 

            double sum3 = (n - 1) / 15;
            sum3 *= sum3 + 1;
            sum3 *= 7.5; //(15/2)   
        }

        static void Main(string[] args)
        {
            long total = 0;
            Stopwatch sw = new Stopwatch();
            for (int i = 0; i < 5; i++)
            {
                sw.Start();
                f(int.MaxValue);
                sw.Stop();
                Console.WriteLine("Run {0} - {1} ms", i + 1, sw.ElapsedMilliseconds);
                total += sw.ElapsedMilliseconds;
                sw.Restart();
            }
            Console.WriteLine("Average - {0} ms", total / 5);
        }
    }
}